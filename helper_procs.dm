/*proc/pick_from_list(list/list)
	var/element = 0
	element = list.len
	if(!element) return
	element = rand (1, element)
	return element*/

obj/proc/move_to(var/x as num, var/y as num)
	while(src.x > x && src.y > y)
		src.x--
		src.y--
		return
	while(src.x < x && src.y > y)
		src.x++
		src.y--
		return
	while(src.x > x && src.y < y)
		src.x--
		src.y++
		return
	while(src.x < x && src.y < y)
		src.x++
		src.y++
		return
	while(src.x > x)
		src.x--
		return
	while(src.x < x)
		src.x++
		return
	while(src.y > y)
		src.y--
		return
	while(src.y < y)
		src.y++
		return