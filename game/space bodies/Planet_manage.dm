datum/building
	var/obj/planet
	var/name = "nothing"
	starbase
		name = "Star Base"
	factory
		var/product
		automaticfactory
			name = "Automatic Factory"
	marinebarracks
		name = "Marine Barracks"
	stock
		name = "Stock"
		datum/var/items = list()
		var/volume
obj/planet/var/list/buildings = list()
obj/planet/proc/Planet_manage(B)
	var/html
	if(src.empire == null)
		html += {"This is free planet!<br />"}
		html += {"Landed ships: <br />"}
		for(var/obj/ship/ship in land_ships)
			html += {"[ship.name]"}
			if(ship.master == usr.ckey)
				html += {"<a href=byond://?src=\ref[ship]&ship_action=take_off>Take off</a><br />"}
				if(istype(ship,/obj/ship/colonist))
					html += {"You can colonize this! <a href=byond://?src=\ref[ship]&ship_action=colonize>Colonize</a><br /><br />"}
	else
		html += {"This planet ruled by [src.empire.e_name]<br /> Planet name is [src.name]."}
		html += {"<a href=byond://?src=\ref[src]&planet_action=news>Read the news</a><br />"}
		html += {"<br />Population: "}
		for(var/datum/race/r in races)
			html += {"[r.name] <a href=byond://?src=\ref[src]&planet_action=build>About</a><br />"}
		if(usr.empire != null && src.empire == usr.empire)
			html += {"You can rename it. <a href=byond://?src=\ref[src]&planet_action=rename>Rename</a><br />"}
			html += {"Buildings list: <br />"}
			for(var/datum/building/Building in buildings)
				html += {"[Building.name] "}
				//if(istype(Building,/datum/building/factory))
				//	html += {"<a href=byond://?src=\ref[Building]&planet_action=product>Choose Production</a> <br />"}
			html += {"<a href=byond://?src=\ref[src]&planet_action=build>Build something</a><br />"}
			html += {"Landed ships: <br />"}
			for(var/obj/ship/ship in land_ships)
				html += {"[ship.name]"}
				if(ship.master == usr.ckey)
					html += {"<a href=byond://?src=\ref[ship]&ship_action=take_off>Take off</a><br />"}
			html += {"<a href=byond://?src=\ref[src]&planet_action=build_ship>Build new ship</a><br />"}
	if(usr.ckey == admin)
		html += {"Admin verbs:<br />"}
		html += {"<a href=byond://?src=\ref[src]&planet_action=destroy>Exterminatus</a><br />"}
	html += {"Resourses on this planet: <br /> Metals - [src.resource_metal] <br /> Cremnium - [src.resource_cremnium] <br /> Plasma - [src.resource_plasma] <br />  Spice - [src.resource_spice] <br />"}
	usr << browse("<HTML>[html]</HTML>", "window=mywindow")

obj/planet/Topic(href, list/href_list)
	..()
	var/planet_action = href_list["planet_action"]
	if(planet_action == "rename")
		src.Rename_Planet()
	if(planet_action == "build")
		src.Build_Something()
	if(planet_action == "build_ship")
		src.Build_Ship()
	if(planet_action == "destroy")
		src.Exterminatus()
	if(planet_action == "news")
		src.News()

obj/planet/proc/Rename_Planet()
	src.name = input("How we name this planet?") as text

obj/planet/proc/Build_Something()
	var/list/Avalible_Buildings = typesof(/datum/building)
	var/T = input(usr,"Choose an building.","Addbuild") as null|anything in Avalible_Buildings
	var/datum/building/B = new T
	buildings += B
	B.planet = src

obj/planet/proc/Build_Ship(ship_type as obj, ai as num, var/a_type as text)
	var/T
	var/list/Avalible_Ships = typesof(/obj/ship)
	if(ai != 1)
		T = input(usr,"Choose a ship for build.","New ship") as null|anything in Avalible_Ships
	else
		T = ship_type
	var/obj/ship/S = new T
	S.x = src.x
	S.y = src.y
	S.z = src.z
	if(ai == 1)
		S.ai = 1
		S.ai_type = a_type
	S.empire = empire
//	world<< empire


obj/planet/proc/Exterminatus()
	var/icon/I = new('standart.dmi',"destroy")
	flick(I,src)
	sleep(10)
	del src

obj/planet/proc/News()
	var/datum/empire/E = empire
	var/html
	var/i
	for(i=1; i <= E.news.len; i++)
		html += {"[E.news[i]]"}
	usr << browse("<HTML>[html]</HTML>", "window=mywindow")
