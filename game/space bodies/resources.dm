obj/planet/var/resource_metal = 0
obj/planet/var/resource_cremnium = 0
obj/planet/var/resource_plasma = 0
obj/planet/var/resource_spice = 0

obj/planet
	New()
		resource_metal = rand(0,1000000)
		resource_cremnium = rand(0,1000000)
		resource_plasma = rand(0,1000000)
		if (prob(5))
			resource_spice = rand(0,100)

mob/verb/check_res()
	for(var/obj/planet/P in view(10))
		usr<< "This planet are contains:"
		usr<<"[P.resource_metal] of metals (in tons)"
		usr<<"[P.resource_cremnium] of cremnium (in tons)"
		usr<<"[P.resource_plasma] of plasma (in tons)"
		usr<<"[P.resource_spice] of spice (in tons)"