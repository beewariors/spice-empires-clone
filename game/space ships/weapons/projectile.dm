obj/projectile
	icon = 'main.dmi'
	icon_state = "boom"
	var/damage

obj/ship/proc/fire_beam(obj/ship/S, damage)
	var/obj/projectile/P = new
	P.loc = locate(src.x,src.y,src.z)
	var/a = S.x
	var/b = S.y
	var/c = S.z
	var/i
	while (P.x != a || P.y != b)
		i++
		step_to (P, locate(a, b, c))
		sleep (0.2)
		if(!S) del P
		if(i > 20) damage = damage - damage * 0.2
	if(a == S.x && b == S.y && c == S.z)
		S.hit(damage)
		if(S.empire != null)
			var/datum/empire/E = S.empire
			spawn() E.change_relations(-10, src.master)
	del P

obj/ship/proc/hit(dam)
	structure_max -= dam
	hpcheck()