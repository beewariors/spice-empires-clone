obj/item/ammo/missile/proc/target_lock(var/obj/ship/B)
	while(src.x != B.x || src.y != B.y)
		step_to (src, locate(B.x, B.y, B.z))
		fuel--
		if (fuel<=0)
			del src
		sleep(2.5)
		if (!B)
			del src
			return
	hit(B)
	del src

obj/item/ammo/missile/proc/hit(var/obj/ship/M)
	M.structure_max -= rand(15,50)
	M.hpcheck()
