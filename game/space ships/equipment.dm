obj/item
		var/mass = 1
obj/item/equipment
	icon = 'graphics/items.dmi'
	icon_state = "equipment"
obj/item/equipment/reactor
	var/capacity
	var/current_capacity
	var/regeneration_per_time
	var/resource_per_time
	pedal_magic_reactor
		capacity = 2500
		regeneration_per_time = 150
		current_capacity = 0
		resource_per_time = 0
		mass = 1
obj/item/equipment/drives/drive
	var/traction
	fucken_pedal_drive
		traction = 8
		mass = 1
obj/item/equipment/drives/hyper_drive
	var/range
	var/energy_per_parsec
	magic_pedal_hdrive
obj/item/equipment/grabber
	var/force
	fucken_pedal_grabber
		force = 150
		mass = 1
obj/item/equipment/shield
	var/field_str
	var/field_def
	var/field_cool_down
	magic_pedal_shield
obj/item/equipment/radar
	var/range
	magic_pedal_radar
obj/item/equipment/scanner
	var/force
	magic_pedal_scanner

obj/ship
	New()
		equipment += new/obj/item/equipment/drives/drive/fucken_pedal_drive(src)
		equipment += new/obj/item/equipment/grabber/fucken_pedal_grabber(src)
		equipment += new/obj/item/equipment/reactor/pedal_magic_reactor(src)
		equipment += new/obj/item/equipment/drives/hyper_drive/magic_pedal_hdrive(src)
		equipment += new/obj/item/equipment/shield/magic_pedal_shield(src)
		equipment += new/obj/item/equipment/radar/magic_pedal_radar(src)
		equipment += new/obj/item/equipment/scanner/magic_pedal_scanner(src)
		check_mass()

obj/item/Topic(href, list/href_list)
	..()
	var/action = href_list["action"]
	if(action == "drop")
		Drop()
	if(action == "hold")
		Move_to_hold()
	if(action == "equip")
		Move_to_equip()

obj/item/proc/Drop()
	var/obj/ship/S = src.loc
	S.hold -= src
	var/x = loc.x
	var/y = loc.y
	var/z = loc.z
	loc = locate(x,y,z)

obj/item/proc/Move_to_hold()
	var/obj/ship/S = src.loc
	if(istype(src,/obj/item/equipment))
		S.equipment -= src
	if(istype(src,/obj/item/equipment))
		S.weapons -= src
	S.hold += src

obj/item/proc/Move_to_equip()
	if(istype(src,/obj/item/equipment))
		var/obj/ship/S = src.loc
		S.hold -= src
		S.equipment += src
	if(istype(src,/obj/item/weapon))
		var/obj/ship/S = src.loc
		S.hold -= src
		S.weapons += src

obj/ship/proc/Update_reactor()
	for(var/obj/item/equipment/reactor/R in equipment)
		if(R.current_capacity < R.capacity)
			R.current_capacity += R.regeneration_per_time
		if(R.current_capacity > R.capacity)
			R.current_capacity = R.capacity

obj/ship/proc/check_mass()
	for(var/obj/item/i in src)
		allmass += i.mass
