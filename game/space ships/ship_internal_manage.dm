obj/planet/var/list/land_ships = list()
obj/ship
	MouseDown(anything,anything,var/c)
		..()
		c = params2list(c)
		if(c["right"])
			world<< "[src]"
			Ship_manage(src)

obj/ship/Topic(href, list/href_list)
	..()
	var/ship_action = href_list["ship_action"]
	if(ship_action == "land")
		Land()
	if(ship_action == "take_off")
		Take_off()
	if(ship_action == "colonize")
		Colonize()

obj/ship/proc/Ship_manage()
	var/html
	html += {"Test Ship Information and Control Panel!<br />"}
	html += {"<a href=byond://?src=\ref[src]&ship_action=land>Land</a><br />"}
	html += {"Weapons:<br />"}
	for(var/obj/item/weapon/W in weapons)
		html += {"[W.name] (volume: [W.mass]) <a href=byond://?src=\ref[W]&action=hold>Move to hold</a><br />"}
	html += {"Drives:<br />"}
	for(var/obj/item/equipment/drives/D in equipment)
		html += {"[D.name] (volume: [D.mass])"}
		if(istype(D,/obj/item/equipment/drives/hyper_drive))
			html += {"Range: "}
		html += {"<a href=byond://?src=\ref[D]&action=hold>Move to hold</a><br />"}
	html += {"Reactors:<br />"}
	for(var/obj/item/equipment/reactor/R in equipment)
		html += {"[R.name] (volume: [R.mass]) (charge = [R.current_capacity]) <a href=byond://?src=\ref[R]&action=hold>Move to hold</a><br />"}
	html += {"Radars:<br />"}
	for(var/obj/item/equipment/radar/Rad in equipment)
		html += {"[Rad.name] (volume: [Rad.mass]) <a href=byond://?src=\ref[Rad]&action=hold>Move to hold</a><br />"}
	html += {"Scanners:<br />"}
	for(var/obj/item/equipment/scanner/S in equipment)
		html += {"[S.name] (volume: [S.mass]) <a href=byond://?src=\ref[S]&action=hold>Move to hold</a><br />"}
	html += {"Grabbers:<br />"}
	for(var/obj/item/equipment/grabber/G in equipment)
		html += {"[G.name] (volume: [G.mass]) <a href=byond://?src=\ref[G]&action=hold>Move to hold</a><br />"}
	html += {"Special systems:<br />"}
	html += {"Other:<br />"}
	html += {"Hold:<br />"}
	for(var/obj/item/I in hold)
		html += {"[I.name] (volume: [I.mass]) "}
		html += {"<a href=byond://?src=\ref[I]&action=drop>Drop</a>"}
		if(istype(I,/obj/item/equipment))
			html += {" <a href=byond://?src=\ref[I]&action=equip>Move to equip</a> <br />"}
		if(istype(I,/obj/item/weapon))
			html += {" <a href=byond://?src=\ref[I]&action=equip>Move to weapons</a> <br />"}
	calc_vol()
	html += {"Free space: [volume]<br />"}
	usr << browse("<HTML>[html]</HTML>", "window=mywindow")

obj/ship/proc/Land()
	for(var/obj/planet/P in range(src,4))
		P.land_ships += src
		src.loc = P
		return

obj/ship/proc/Take_off()
	var/obj/planet/P = src.loc
	P.land_ships -= src
	var/x = loc.x
	var/y = loc.y
	var/z = loc.z
	loc = locate(x,y,z)

obj/ship/proc/Colonize()
	var/obj/planet/P = src.loc
	if(!istype(P,/obj/planet/gas_gigant))
		if(src.empire != null)
			if(P.empire == null)
				P.empire = src.empire
				P.empire.planet += P
				//world<< "Now this planet belongs to [src.empire.e_name] empire!"
				P.land_ships -= src
				P.empire.new_news("colonize", P)
				P.is_occupied = 1
				del src
			else
				world<< "This planet already colonized, Commander!"
		else
			usr<< "You have no empire, koa!"
	else
		usr<< "Gas giant cannot be colonized!"