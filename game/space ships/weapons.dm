obj/item/weapon/beam
	var/energy_costs
	var/damage
obj/item/weapon/beam/magic_laser
	energy_costs = 200
	damage = 5
obj/item/weapon/missile
	var/caliber = 1
obj/item/weapon/missile/magic_rocket_launcher
obj/item/weapon/field
obj/item/weapon/gauss
obj/item/weapon/bomb
obj/item/ammo
	var/damage
obj/item/ammo/missile
	var/fuel = 20
	var/caliber
	icon = 'graphics/main.dmi'
	icon_state = "missile"
obj/item/ammo/missile/base_missile
	caliber = 1
	damage = 15

obj/ship
	New()
		..()
		weapons += new/obj/item/weapon/beam/magic_laser(src)
		weapons += new/obj/item/weapon/missile/magic_rocket_launcher(src)
		hold += new/obj/item/ammo/missile/base_missile(src)