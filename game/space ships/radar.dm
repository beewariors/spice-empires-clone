client
	var
		obj/radar

world/New()
	..()
	Update_Radar()

client/New()
	var/obj/O = new   // This code creates the radar object and places it on the client's screen. Note how the object 'follows' the mob around as he moves. This is because the object is 'attached' to the client screen.
	O.icon = icon('graphics/ships/big ships.dmi',"radar_test")
	O.screen_loc = "NORTH - 3,EAST - 3"  // Place the radar at the top-right corner of the screen
	O.layer = 50  // Set it to a high layer so nothing appears over it
	src.screen += O
	src.radar = O
	..()

proc/Update_Radar()
	for(var/client/C)  // Loop through all non-npc players...
		if(C.radar)
			var/icon/R = icon('graphics/ships/big ships.dmi',"radar_test")  // Create an icon to draw on
			R.DrawBox(rgb(0,128,0),0,0,128,128)  // Clear the radar icon with a dark-ish green
			R.DrawBox(rgb(255,255,255), 64, 64-3, 64, 64+3)
			R.DrawBox(rgb(255,255,255), 64-3, 64, 64+3, 64)
			for(var/obj/planet/M in world)  // Search through all of the mobs...
				if(get_dist(C.mob,M) < 120 && C.mob.z == M.z && C.mob != M)  // If the mob is within 10 squares of the player, is on the same z level, and isn't itself (don't want to place yourself on it, do you?)
					var/x = 64-(C.mob.x - M.x)+1
					var/y = 64-(C.mob.y - M.y)+1
					R.DrawBox(M.team, x-1, y-1, x+1, y+1)  // Draw the blip at the proper x/y coordinates
			for(var/obj/ship/M in world)  // Search through all of the mobs...
				if(get_dist(C.mob,M) < 120 && C.mob.z == M.z && C.mob != M)  // If the mob is within 10 squares of the player, is on the same z level, and isn't itself (don't want to place yourself on it, do you?)
					var/x = 64-(C.mob.x - M.x)+1
					var/y = 64-(C.mob.y - M.y)+1
					R.DrawBox(M.team, x-1, y-1, x+1, y+1)  // Draw the blip at the proper x/y coordinates
			C.radar.icon = R  // Set the new icon onto the radar
	spawn(5)
		Update_Radar()  // Refresh the radar every 1/2 second  (feel free to play with the update speed!)