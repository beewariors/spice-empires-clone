mob/var/has_ship=0
obj/ship/var/team = rgb(0,0,255)
obj/ship/var/list/weapons = list()
obj/ship/var/list/equipment = list()
obj/ship/var/list/special_systems = list()
obj/ship/var/list/arts = list()
obj/ship/var/list/hold = list()
obj/ship/var/list/artefacts = list()
obj/ship/var/structure_max = 25 //25 60 120 250 500 1200
obj/ship/var/allmass
//obj/ship/freighter
//obj/ship/shuttle
//obj/ship/bombard
obj/ship/frigate
	icon = 'graphics/ships/small ships.dmi'
	icon_state = "frigate"
obj/ship/destroyer
	icon = 'graphics/ships/small ships.dmi'
	icon_state = "destroyer"
obj/ship/cruiser
	icon = 'graphics/ships/middle ships.dmi'
	icon_state = "cruiser"
obj/ship/battleship
	icon = 'graphics/ships/middle ships.dmi'
	icon_state = "battleship"
obj/ship/titan
	icon = 'graphics/ships/big ships.dmi'
	icon_state = "titan"
obj/ship/doomstar
	icon = 'graphics/ships/big ships.dmi'
	icon_state = "doomstar"
obj/ship/outpost
	icon = 'graphics/ships/middle ships.dmi'
	icon_state = "outpost_ship"
obj/ship/colonist
	icon = 'graphics/ships/middle ships.dmi'
	icon_state = "colony_ship"
obj/ship/transport
	icon = 'graphics/ships/middle ships.dmi'
	icon_state = "transport"

obj/ship/var/master=0
obj/ship/var/empire
obj/ship/var/flying=0
obj/ship/var/hud_intent = 0
obj/ship/var/hyperdrive_heat=0
obj/ship/var/reloading = 0
obj/ship/proc/reload()
	while(reloading >= 1)
		sleep(15)
		reloading = 0

obj/ship/proc/drop_something()
	world<< "DROP"
	for(var/obj/item/equipment/e in equipment)
		e.x = x
		e.y = y
		e.z = z

obj/ship/proc/hpcheck()
	if(structure_max <= 0)
		drop_something()
		for(var/mob/O in world)
			if (master != O) continue
			//O.shhud()
			master = 0
			O.has_ship = 0
			usr<< "You no more controle this ship"
		del src
obj/ship/proc/scan()
	usr<< "click!"
	var/html= {"structure_max points = [structure_max]"}
	usr << browse(html, "window=mywindow")
obj/ship/proc/hyperdrive_cool_down(heat)
	while(hyperdrive_heat>0)
		hyperdrive_heat-=5
		sleep(5)

obj/ship/proc/fly()
obj/ship
	icon = 'main.dmi'
	icon_state = "Ship"
	layer=OBJ_LAYER+1
	fly(a,b)
		var/t
		for(var/obj/item/equipment/drives/drive/d in equipment)
			t += d.traction
		if(t == null)
			usr<< "This ship cant move!"
			return
		while(flying == 1)
			move_to(a,b)
			//step_to (src, locate(a,b,c))
			if(src.x == a && src.y == b)
				usr<< "Prileteley"
				flying = 0
			sleep(allmass/t)


mob/verb/create_test_ship()
	if(has_ship == 0)
		world<< "[usr] create a new ship for himself"
		new /obj/ship(locate(src.x,src.y,src.z))
	else
		usr<< "You are already have a ship, dumb"
	has_ship=0
obj/ship/DblClick(atom/O as obj)
	for (var/obj/ship/M in view(O,0))
		if (M.master == 0)
			world << "[usr] are now control this ship"
			M.name = usr.ckey + "'s frigate"
			M.master = usr
			usr.shhud()
			//usr.show=1


turf/space/Click(var/turf/space/O)
	for (var/obj/ship/M in world)
		if (M.master == usr)
			if(M.hud_intent == 1)
				usr<< "proverka"
				if (M.flying==1)
					usr<< "Wait before you reach your destignation point or press stop"
				if (M.flying==0)
					M.flying=1
					M.fly(O.x,O.y,M.z)


obj/ship/MouseDown(obj/ship/S,an2,var/c)
	..()
	c = params2list(c)
	if(c["left"])
		for (var/obj/ship/M in world)
			if (M.z != src.z) continue
			if (M.master == usr)
				if(M.hud_intent == 3)
					usr<< "click!"
					src.scan()
				if(M.hud_intent == 4)
					if(M.reloading == 0)
						for(var/obj/item/weapon/W in M.weapons)
							if(istype(W,/obj/item/weapon/missile))
								for(var/obj/item/ammo/missile/base_missile/b in M.hold)
									if(b)
										world<< "[b]"
										//src = src.loc
										M.hold -= b
										var/x = M.x
										var/y = M.y
										var/z = M.z
										b.loc = locate(x,y,z)
										usr<< "fire missile"
										//M.reloading += 1
										//M.reload()
										if(M.dir == NORTH)
											for(var/i=0; i<4; i++)
												b.y++
												sleep(2.5)
										if(M.dir == SOUTH)
											for(var/i=0; i<4; i++)
												b.y--
												sleep(2.5)
										if(M.dir == EAST)
											for(var/i=0; i<4; i++)
												b.x++
												sleep(2.5)
										if(M.dir == WEST)
											for(var/i=0; i<4; i++)
												b.x--
												sleep(2.5)
										b.target_lock(src)
										return
									if(!b)
										usr<< "No ammo"
							if(istype(W,/obj/item/weapon/beam))
								for(var/obj/item/weapon/beam/B in M.weapons)
									if(B.energy_costs < M.calc_energy())
										M.fire_beam(src, B.damage)
										world<< "Fire! [M.calc_energy()]"
										var/i
										for(var/obj/item/equipment/reactor/R in M.equipment)
											i += 1
										for(var/obj/item/equipment/reactor/Rr in M.equipment)
											Rr.current_capacity -= B.energy_costs / i
									else
										world<< "No! Too low energy!"
				//fire()
			//if	(M.flying > 1)
				//M.flying = 0

obj/item/Click()
	for (var/obj/ship/M in view())
		if (M.master == usr)
			if(M.hud_intent == 5)
				for(var/obj/item/equipment/grabber/g in M.equipment)
					if(g)
						while (src.x != M.x || src.y != M.y)
							if(get_dist(src,M) > 5)
								return
							step_to(src, locate(M.x, M.y, src.z))
							sleep(10)
						usr<< "You grab item [src]"
						src.loc = M
						M.hold += src
						return
				usr<< "You can't grab items without grabber onboard"

obj/ship/var/volume
obj/ship/proc/calc_vol()
	var/allmass
	for(var/obj/item/i in equipment)
		allmass += i.mass
	for(var/obj/item/i in hold)
		allmass += i.mass
	volume = structure_max
	volume -= allmass

obj/ship/proc/calc_energy()
	var/all_energy
	for(var/obj/item/equipment/reactor.R in equipment)
		all_energy += R.current_capacity
	return all_energy

obj/ship/proc/jump(var/num as num)
	var/time = rand(300,1200)
	var/obj/cloud/cin/I = new
	I.x = src.x
	I.y = src.y
	I.z = src.z
	spawn() I.life(time)
	z = num
	if(master != 0)
		usr.z = num
	var/obj/cloud/cout/O = new
	O.x = src.x
	O.y = src.y
	O.z = src.z
	spawn() O.life(time)
	usr << "You are on [num] star!"
	hyperdrive_heat += 200
	hyperdrive_cool_down(hyperdrive_heat)

obj/cloud
	icon = 'main.dmi'
	cin
		icon_state = "cloud_in"
	cout
		icon_state = "cloud_out"
obj/cloud/proc/life(var/time as num)
	sleep(time)
	del src