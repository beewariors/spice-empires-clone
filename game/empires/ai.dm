mob/verb/spawn_ai()
	if(src.ckey != admin)
		world<< "You are not admin"
	else
		//create_empire(1)
		var/count = input("how many ships?") as num
		var/i = 0
		while(i<count)
			i++
			var/obj/ship/S = new
			S.x = 20
			S.y = 20
			S.z = 1
			S.ai = 1

/*datum/empire
	New()
		if(ai > 0)
			spawn() empire_ai_controller()*/

var/ai_time_step = 60

datum/empire/proc/empire_ai_controller()
	if(ai > 0)
		while(1)
			spawn () built_controller()
			spawn () colonize_controller()
			spawn () resourse_managment_controller()
			spawn () diplomacy_controller()
			spawn () exploring_controller()
			spawn () war_controller()
			spawn () planner_controller()
			sleep(ai_time_step)
			//spawn() empire_ai_controller()
	sleep(ai_time_step)
	spawn() empire_ai_controller()

obj/ship/var/ai_type = null
datum/empire/var/ai_list_need_ship_explorer
datum/empire/var/ai_list_need_ship_colonize
datum/empire/var/ai_list_need_ship_transport
datum/empire/var/ai_list_need_ship_warrior

datum/empire/proc/built_controller()
	while(ai_list_need_ship_explorer > 0)
		for(var/obj/planet/P in planet)
			P.Build_Ship(/obj/ship/frigate, 1, "explorer", src)
			ai_list_need_ship_explorer--
			if(ai_list_need_ship_explorer != 0)
				return
	while(ai_list_need_ship_colonize > 0)
		for(var/obj/planet/P in planet)
			P.Build_Ship(/obj/ship/colonist, 1, "colonize", src)
			ai_list_need_ship_colonize--
			if(ai_list_need_ship_colonize != 0)
				return
	while(ai_list_need_ship_warrior > 0)
		for(var/obj/planet/P in planet)
			P.Build_Ship(/obj/ship/battleship, 1, "warrior", src)
			ai_list_need_ship_warrior--
			if(ai_list_need_ship_warrior != 0)
				return
	//battleship

datum/empire/var/ai_list_known_planets = list()
datum/empire/var/ai_list_unknown_planets = list()
datum/empire/var/ai_list_known_planets_counter = 0
datum/empire/proc/exploring_controller()


datum/empire/proc/colonize_controller()
datum/empire/proc/resourse_managment_controller()
datum/empire/proc/diplomacy_controller()
datum/empire/var/emeny_targets = list()
datum/empire/proc/war_controller()
	emeny_targets = null
	for(var/i=2; i <= relationship_list.len; i+=2)
		if(relationship_list[i] < 100)
			var/m = relationship_list[i-1]
			if(istype(m,/datum/empire))
				for(var/obj/ship/S in world)
					if(S.empire == m)
						emeny_targets += S
			else
				for(var/obj/ship/S in world)
					if(S.master == m)
						emeny_targets += S
datum/empire/proc/planner_controller()
	var/explorer = 0
	var/colonize = 0
	//var/transport = 0
	var/warrior = 0
	for (var/obj/ship/S in world)
		if(S.ai_type == "explorer")
			explorer++
		if(S.ai_type == "colonize")
			colonize++
		/*if(S.ai_type == "transport")
			transport++*/
		if(S.ai_type == "warrior")
			warrior++
	if(explorer < 10)
		ai_list_need_ship_explorer++
	if(colonize < 10)
		ai_list_need_ship_colonize++
	if(warrior < 10)
		ai_list_need_ship_warrior++
		//ai_list_known_planets_counter--
	world<< "Plan plan plan [ai_list_need_ship_explorer] [ai_list_need_ship_colonize]"

obj/ship/var/ai = 0
obj/ship/var/ai_has_tack = 0


obj/ship/proc/ai_explorer()
	var/datum/empire/E = empire
	var/obj/planet/target
	if(ai_has_tack == 0)
		ai_has_tack = 1
		target = pick(E.ai_list_unknown_planets)
		if(src.z != target.z)
			jump(target.z)
		retry
		flying = 1
		fly(target.x,target.y)
		if(get_dist(src,target) > 10)
			goto retry
		E.ai_list_unknown_planets -= target
		E.ai_list_known_planets += target
		E.ai_list_known_planets_counter++
		ai_has_tack = 0
		return
obj/ship/proc/ai_colonize()
	var/datum/empire/E = empire
	var/obj/planet/target
	if(ai_has_tack == 0)
		var/datum/empire/list/cruch = E.ai_list_known_planets
		if(cruch.len != 0)
			ai_has_tack = 1
			target = pick (E.ai_list_known_planets)
				//world<< "[Pk.name] [Pk.x] [Pk.y] [Pk.z]"
			if(target.is_occupied == 0)
				if(target.empire == null)
					if(!istype(target,/obj/planet/gas_gigant))
						if(src.z != target.z)
							jump(target.z)
						retry
						flying = 1
						fly(target.x, target.y)
						if(get_dist(src, target) > 2)
							goto retry
						if(target.empire == null)
							//world<< "Before Land"
							Land()
							//world<< "After Land"
							if(target.empire == null)
								//world<< "Before Colonize"
								for(var/datum/empire/Eb in empires)
									Eb.ai_list_known_planets -= target
								E.ai_list_known_planets -= target
								E.ai_list_known_planets_counter--
								Colonize()
								Take_off()
								ai_has_tack = 0
							else
								Take_off()
								ai_has_tack = 0
						else
							ai_has_tack = 0
				else
					ai_has_tack = 0
			else
				ai_has_tack = 0

obj/ship/proc/ai_transport()
obj/ship/proc/ai_warrior()
	var/datum/empire/E = empire
	var/obj/ship/target_ship
	if(ai_has_tack == 0)
		if(E.emeny_targets != null)
			ai_has_tack = 1
			target_ship = pick(E.emeny_targets)
			if(src.z != target_ship.z)
				jump(target_ship.z)
			flying = 1
			fly(target_ship.x,target_ship.y)
	ai_has_tack = 0

obj/ship/proc/ai_ship_controller()
	if(ai == 1)
		if(ai_has_tack == 0)
			if(ai_type == "explorer")
				ai_explorer()
			if(ai_type == "colonize")
				ai_colonize()
			if(ai_type == "warrior")
				ai_warrior()
			//ai_has_tack = 1
			//var/tgx = rand(1,299)
			//var/tgy = rand(1,299)
			//var/tgz = rand(1,55)
			//world<< "fly to coords [tgx] [tgy] [tgz]"
			//if(src.z != tgz)
			//	jump(tgz)
			//flying=1
			//fly(tgx, tgy)
			//world<< "afterjump"
			//while(src.x != tgx || src.y != tgy)
			//	fly(tgx, tgy)
			//	sleep(10)
			//ai_has_tack = 0

mob/verb/ship_count()
	var/i
	var/j
	for(var/obj/ship/frigate/S in world)
		i++
	for(var/obj/ship/colonist/S in world)
		j++
	usr<< i
	usr<< j

