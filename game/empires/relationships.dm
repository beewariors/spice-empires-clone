datum/empire/var/list/relationship_list = list()

mob
	Login()
		..()
		for(var/datum/empire/E in empires)
			var/i
			for(i=1; i <= E.relationship_list.len; i++)
				if(src.ckey == E.relationship_list[i])
					return
			E.new_guy(usr.ckey)

datum/empire
	New()
		..()
		for(var/datum/empire/E in empires)
			if(E != src)
				relationship_list += E
				relationship_list += 100
				E.relationship_list += src
				E.relationship_list += 100
		for(var/mob/M in world)
			relationship_list += M
			relationship_list += 100


datum/empire/proc/change_relations(count, var/mob/mob, var/datum/empire/empire)
	if(mob != null)
		var/i = relationship_list.Find(mob)
		relationship_list[i+1] += count
	if(empire != null)
		var/i = relationship_list.Find(empire)
		relationship_list[i+1] += count

datum/empire/proc/new_guy(ckey)
	relationship_list += ckey
	world<< "Hi guy!"

mob/verb/relations_list()
	var/html
	var/i
	for(var/datum/empire/E in empires)
		html += {"Empire [E.e_name] relationship list: <br>"}
		for(i=1; i <= E.relationship_list.len-1; i++)
			var/m = E.relationship_list[i]
			var/mob/out_mob
			var/datum/empire/out_empire
			if(istype(m,/datum/empire))
				out_empire = E.relationship_list[i]
				html += {"[out_empire.e_name]"}
			else
				out_mob = E.relationship_list[i]
				html += {"[out_mob.ckey]"}
			i++
			var/relate = E.relationship_list[i]
			html += {": [relate]<br>"}
	usr << browse("<HTML>[html]</HTML>", "window=mywindow")