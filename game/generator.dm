
var/levels = 60
var/map_length = 300
var/generator = 0
//var/generator_map_place = locate(rand(10,30),rand(10,30),generator_floor)
var/generator_floor = 1
var/maxbodyes = 9
var/generator_random = 0

proc/generategalaxy()
	/*var/generator_random[2]
	generator_random[1] = new/obj/planet/barren(locate(rand(10,30),rand(10,30),generator_floor))
	generator_random[2] = new/obj/planet/terra(locate(rand(10,30),rand(10,30),generator_floor))*/
	world<< "Generating new universe..."
	generator_floor = 1
	while(generator_floor < levels+1)
		world<< "creating new star"
		//new/obj/sun(locate(map_length/2,map_length/2,generator_floor))
		var/obj/sun/S = new(locate(map_length/2,map_length/2,generator_floor))
		S.map_x = rand(0,600); S.map_y = rand(0,600)
		world<< "new star create"
		//sleep(1)
		for (var/j = 0; j < rand(0,maxbodyes+1); j++)
			generator_random = rand(0,100)
			if(generator_random <= 39)
				new /obj/planet/barren(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new barren on a star with number [generator_floor]"
			if(generator_random >= 40 && generator_random <=59 )
				new /obj/planet/gas_gigant(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new gas_gigant on a star with number [generator_floor]"
			if(generator_random >= 60 && generator_random <=69)
				new /obj/planet/poison(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new poison on a star with number [generator_floor]"
			if(generator_random >= 70 && generator_random <=79)
				new /obj/planet/radioactive(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new radioactive on a star with number [generator_floor]"
			if(generator_random >= 80 && generator_random <=85)
				new /obj/planet/desert(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new desert on a star with number [generator_floor]"
			if(generator_random >= 86 && generator_random <=88)
				new /obj/planet/arid(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new arid on a star with number [generator_floor]"
			if(generator_random >= 89 && generator_random <=91)
				new /obj/planet/swamp(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new swamp on a star with number [generator_floor]"
			if(generator_random >= 92 && generator_random <=94)
				new /obj/planet/tundra(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new tundra on a star with number [generator_floor]"
			if(generator_random >= 95 && generator_random <=97)
				new /obj/planet/terra(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new terra on a star with number [generator_floor]"
			if(generator_random >= 98 && generator_random <=99)
				new /obj/planet/oceanic(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new oceanic on a star with number [generator_floor]"
			if(generator_random == 100)
				new /obj/planet/gaia(locate(rand(10,300),rand(10,300),generator_floor))
				world<< "create new gaia on a star with number [generator_floor]"
		generator_floor++

mob/verb/local_scan()
	scan_system()
mob/proc/scan_system()
	var/list/html
	for (var/obj/planet/P in world)
		if (P.z != src.z) continue
		html += {"Planet_type:[P.name] X:[P.x] Y:[P.y] <br />"}
		usr << browse("<HTML>[html]</HTML>", "window=mywindow")

mob/verb/show_map()
	starmap()
mob/proc/starmap()
	src << browse_rsc('graphics/sun_icons.dmi', "sun")
	var/html
	//var/backgrou
	for(var/obj/sun/S in world)
		html +=  {" <body bgcolor="#000000"><div style="position: absolute; top: [S.map_x]px; left: [S.map_y]px"><"<font color="red">[S.z]</font>"></div><div style="position: absolute; top: [S.map_x]px; left: [S.map_y]px"><img src="sun" /></div>"}
		usr << browse("<HTML>[html]</HTML>", "window=mywindow")
//mob/proc/starmap()