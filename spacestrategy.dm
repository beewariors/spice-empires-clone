/*
	These are simple defaults for your project.
 */
//TODO LIST: TRADING GENES, Lifecycle
world
	fps = 60		// 25 frames per second
	icon_size = 32	// 32x32 icon size by default

	view = 10		// show up to 6 tiles outward from center (13x13 view)
	tick_lag = 0.05

#define DEBUG

// Make objects move 8 pixels per tick when walking
client/show_popup_menus = 0
world/turf = /turf/space

mob
	..()
	step_size = 32
	icon = 'main.dmi'
	icon_state = "visor"
	name = "visor"
	verb
		docs()
			switch(alert("Which doc you want to see?"," ","Devblog","Guide","Consept"))
				if ("Devblog")
					src << browse('docs/devblog.html', "window=mywindow")
				if ("Guide")
					src << browse('docs/guides.html', "window=mywindow")
				if ("Consept")
					src << browse('docs/consept.html', "window=mywindow")
		what_a_star()
			usr<< "You are on [usr.z] star now"
		say(message as text)
			var/index = findtext(message, "�")
			while(index)
				message = copytext(message, 1, index) + "&#255;" + copytext(message, index+1)
				index = findtext(message, "�")
			world<< "[usr] says, [message]"
		destroy_galaxy()
			set hidden=1
			for (var/O as obj)
				del O
			generator = 0
			has_ship=0
			world<< "Destroing the galaxy..."
	Logout()
		world<< "[usr] logout"
	Stat()
		..()
		stat("x",src.x)
		stat("y",src.y)
		//statpanel("Inventory","y",src.y)

obj
	step_size = 32

turf
	space
		icon = 'graphics/main.dmi'
		icon_state = "space"
		New()
			icon_state = pick("space","space2","space3","space4","space5","space6","space7","space8")
	start

/*verb/say_act(message as text)
	var/index = findtext(message, "y")
	while(index)
		message = copytext(message, 1, index) + "&#255;" + copytext(message, index+1)
		index = findtext(message, "y")
	say(message)*/

var
	mob/g_Player
	icon/ch = new('pointer.dmi')

mob
	Login()
		g_Player = usr
		g_Player.client.mouse_pointer_icon = ch // manual targeting on by default
		..()